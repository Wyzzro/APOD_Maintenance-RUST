<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">

<h3 align="center">Maintenance - NASA Astronomy Picture of the Day</h3>

  <p align="center">
Voici l'application Rust Rocket qui sera exécutée durant les maintenances de l´application principale.
  </p>
</div>

![](maintenance_page.png)

### Développer Avec

* [RUST](https://www.rust-lang.org/fr)
* [Rocket](https://rocket.rs/)
* [Tera](https://tera.netlify.app/)
* [Bootstrap](https://getbootstrap.com/)

<!-- GETTING STARTED -->

## Mise en Place

Voici la démarche à faire pour installer le repo et mettre en place le site web.

### Prerequisites

1. Installer Rust
  ```sh
  sudo pacman -S rust
  ```

### Installation

1. Cloner le Repo
   ```sh
   git clone https://gitlab.com/Wyzzro/APOD_Maintenance-RUST.git
   ```
2. Aller dans le dossier
   ```sh
   cd APOD_Maintenance-RUST/
   ```
3. Mettre à jour les paquets
   ```sh
   cargo update
   ```
4. Installer les paquets, compiler et lance le serveur
   ```sh
   cargo run
   ```
5. Ouvrez votre navigateur et aller à l'adresse suivante :
   ```sh
   localhost:8000
   ```

<!-- CONTACT -->

## Contact

Le Touzic Ethan - ethan.letouzic@hotmail.fr
