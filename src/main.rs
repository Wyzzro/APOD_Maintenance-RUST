#[macro_use] extern crate rocket;

use rocket_dyn_templates::Template;
use serde::Serialize;

#[derive(Serialize)]
pub struct TemplateContextError {
    pub title: String,
    pub messages: String,
    pub error: bool,
}

#[catch(404)]
fn not_found() -> Template {
    let context = TemplateContextError { title: "Erreur 404".to_string(), messages: "La page que vous cherchez n'existe pas !".to_string(), error: true };
    Template::render("index", &context)
}

#[catch(500)]
fn internal_error() -> Template {
    let context = TemplateContextError { title: "Erreur 500 !".to_string(), messages: "Une erreur interne au serveur est survenue, veuillez réessayer plus tard !".to_string(), error: true };
    return Template::render("index", &context);
}

#[get("/")]
fn index() -> Template {
    let context = TemplateContextError { title: "".to_string(), messages: "".to_string(), error: false };
    Template::render("index", context)
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .register("/", catchers![not_found, internal_error])
        .mount("/", routes![index])
        .attach(Template::fairing())
}
